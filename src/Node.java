
import java.util.*;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }

   // https://git.wut.ee/i231/home5/src/branch/master/src/Node.java
   public static Node parsePostfix (String s) {
      // 5. Suluesituses lugeda lubatavaks tühikute kasutamine loetavuse parandamiseks (näiteks peale koma on see loomulik).
      // https://regex101.com/
      s = s.replaceAll("(\\s+)?,(\\s+)?", ",");
      s = s.replaceAll("(\\s+)?\\((\\s+)?", "(");
      s = s.replaceAll("(\\s+)?\\)(\\s+)?", ")");

      if (s.contains(",,")) {
         throw new RuntimeException(String.format("Double commas in %s", s));
      } else if (s.contains("\t")) {
         throw new RuntimeException(String.format("Tab char in %s", s));
      } else if (s.contains("()")) {
         throw new RuntimeException(String.format("Empty subtree in %s", s));
      } else if (s.contains(" ")) {
         throw new RuntimeException(String.format("Unexpected space in %s", s));
      } else if (s.contains("((") && s.contains("))")) {
         throw new RuntimeException(String.format("Double brackets in %s", s));
      } else if (s.contains(",") && !(s.contains("(") && s.contains(")"))) {
         throw new RuntimeException(String.format("Two roots in %s", s));
      } else if (s.contains("(,") || s.contains(",)")) {
         throw new RuntimeException(String.format("Blank node %s", s));
      }

      String inputString = s.replaceAll("\\s+", "");
      StringTokenizer tokens = new StringTokenizer(inputString, "(,)", true);

      Stack<Node> stack = new Stack<>();
      Node node = new Node(null, null, null);
      boolean replacingRoot = false;

      while(tokens.hasMoreTokens()) {
         String token = tokens.nextToken();
         switch (token) {
            case "(":
               if (replacingRoot) {
                  handleNotRootNodeException(token, s);
               }
               stack.push(node);
               node.firstChild = new Node(null, null, null);
               node = node.firstChild;
               break;
            case ")":
               if (replacingRoot) {
                  handleNotRootNodeException(token, s);
               }
               node = stack.pop();
               if (stack.size() == 0) {
                  replacingRoot = true;
               }
               break;
            case ",":
               if (replacingRoot) {
                  handleNotRootNodeException(token, s);
               }
               node.nextSibling = new Node(null, null, null);
               node = node.nextSibling;
               break;
            default:
               node.name = token;
               break;
         }
      }

      return node;
   }

   private static void handleNotRootNodeException(String symbol, String input) throws RuntimeException {
      throw new RuntimeException(String.format("Unexpected token \"%s\" instead of root node in %s", symbol, input));
   }

   // https://git.wut.ee/i231/home5/src/branch/master/src/Node.java
   public String leftParentheticRepresentation() {
      StringBuilder sb = new StringBuilder();
      sb.append(this.name);
      if (this.firstChild != null) {
         sb.append("(");
         sb.append(this.firstChild.leftParentheticRepresentation());
         sb.append(")");
      }

      if (this.nextSibling != null) {
         sb.append(",");
         sb.append(this.nextSibling.leftParentheticRepresentation());
      }
      return sb.toString();
   }

   @Override
   // https://enos.itcollege.ee/~jpoial/algorithms/examples/TreeNode.java
   public String toString() {
      return leftParentheticRepresentation();
   }

   public static void main (String[] param) {
      String s = "(( B1 , C ) A , C ) A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)

//      String s2 = "(((5, 1)-, 7)*, (6, 3)/ )+";
//      Node t2 = Node.parsePostfix(s2);
//      String v2 = t2.leftParentheticRepresentation();
//      System.out.println (s2 + " ==> " + v2); // (((5, 1)-, 7)*, (6, 3)/ )+ ==> + (* (- (5, 1), 7), / (6, 3))
   }
}

